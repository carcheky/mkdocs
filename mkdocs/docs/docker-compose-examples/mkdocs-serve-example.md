# Mkdocs Serve example

````yml
services:
  # example serve in http://mkdocs.localhost
  traefik:
    image: traefik:v2.0
    command: --api.insecure=true --providers.docker
    ports:
      - '80:80'
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
  mkdocs:
    image: carcheky/mkdocs:latest
    labels:
      - "traefik.http.routers.mkdocs.rule=Host(`mkdocs.localhost`)"
      - "traefik.http.services.mkdocs.loadbalancer.server.port=8080"
    volumes:
      - ./mkdocs/mkdocs.yml:/mkdocs/mkdocs.yml:rw
      - ./mkdocs/docs/:/mkdocs/docs/:rw
      - ./mkdocs/pdf/:/mkdocs/pdf/:rw
````