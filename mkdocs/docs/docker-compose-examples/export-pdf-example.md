# Export pdf example

````yml
services:
  # example export pdf
  export_pdf:
    image: carcheky/mkdocs:latest
    environment:
      - ENABLE_PDF_EXPORT=1
    volumes:
      - ./mkdocs/pdf-subfolder.yml:/mkdocs/mkdocs.yml:rw
      - ./mkdocs/docs/:/mkdocs/docs/:rw
      - ./mkdocs/pdf/:/mkdocs/pdf/:rw
````