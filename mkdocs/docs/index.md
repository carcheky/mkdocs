# Home

inspired by [octadocs](https://github.com/octadocs/octadocs(octadocs)

## Docker image for mkdocs

````yml
# PDF Documentation

Docker compose example

````yml
services:
  # example serve in http://mkdocs.localhost
  traefik:
    image: traefik:v2.0
    command: --api.insecure=true --providers.docker
    ports:
      - '80:80'
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
  mkdocs:
    image: carcheky/mkdocs:latest
    labels:
      - "traefik.http.routers.mkdocs.rule=Host(`mkdocs.localhost`)"
      - "traefik.http.services.mkdocs.loadbalancer.server.port=8080"
    volumes:
      - ./mkdocs/mkdocs.yml:/mkdocs/mkdocs.yml:rw
      - ./mkdocs/docs/:/mkdocs/docs/:rw
      - ./mkdocs/pdf/:/mkdocs/pdf/:rw
  # example export pdf
  export_pdf:
    image: carcheky/mkdocs:latest
    environment:
      - ENABLE_PDF_EXPORT=1
    volumes:
      - ./mkdocs/pdf-subfolder.yml:/mkdocs/mkdocs.yml:rw
      - ./mkdocs/docs/:/mkdocs/docs/:rw
      - ./mkdocs/pdf/:/mkdocs/pdf/:rw
````
