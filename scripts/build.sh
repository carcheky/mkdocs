#!/bin/bash
set -eux

version=1.0.3

function build () {
    tag=$1
    docker build --progress plain --pull --rm -f "builds/mkdocs-${tag}/Dockerfile" -t carcheky/mkdocs:${tag} "builds/mkdocs-${tag}/"
}

function push () {
    tag=$1
    docker build --progress plain --pull --rm -f "builds/mkdocs-${tag}/Dockerfile" -t carcheky/mkdocs:${tag} -t carcheky/mkdocs:${tag}-${version} "builds/mkdocs-${tag}/"
    docker image push carcheky/mkdocs:${tag}
    docker image push carcheky/mkdocs:${tag}-${version}
}

function all(){
    # echo "building"
    # bash scripts/build.sh build pdf
    # bash scripts/build.sh build pdf-octadocs
    echo "pushing"
    bash scripts/build.sh push pdf
    bash scripts/build.sh push pdf-octadocs
}

$@