#!/bin/bash
set -eux

function serve () {
    mkdocs serve --dev-addr=0.0.0.0:8080
}

$@